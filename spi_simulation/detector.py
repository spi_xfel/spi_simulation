"""
Detector coordinates in reciprocal space
Author: Sergey Bobkov
"""

from typing import Tuple
import numpy as np

from spi_simulation import config


def _compute_polarization(polarization: config.Polarization,
                          px_grid: np.ndarray,
                          py_grid: np.ndarray,
                          norm_grid: np.ndarray) -> np.ndarray:
    if polarization == config.Polarization.X:
        return 1. - (px_grid*px_grid)/(norm_grid*norm_grid)
    if polarization == config.Polarization.Y:
        return 1. - (py_grid*py_grid)/(norm_grid*norm_grid)
    if polarization == config.Polarization.none:
        return 1. - (px_grid*px_grid + py_grid*py_grid)/(2*norm_grid*norm_grid)

    raise ValueError('Wrong polarisation value: {}'.format(polarization))


class Detector():
    'Generates pixel coordinates in reciprocal space'

    def __init__(self, conf: config.Configuration):
        """
        Keyword arguments:
        conf -- configuration
        """

        self.conf = conf
        self._compute_coords()

    def _compute_coords(self):
        q_scaling = 2 * np.pi / self.conf.experiment.wavelength

        x_vals = np.arange(self.conf.detector.resolution.x,
                           dtype=float) - self.conf.detector.center.x
        y_vals = np.arange(self.conf.detector.resolution.y,
                           dtype=float) - self.conf.detector.center.y

        self.x_grid, self.y_grid = np.meshgrid(x_vals, y_vals)

        px_grid = self.conf.detector.pixel_size*self.x_grid
        py_grid = self.conf.detector.pixel_size*self.y_grid

        norm_grid = np.sqrt(px_grid**2 + py_grid**2 + self.conf.detector.distance**2)

        pixel_polar = _compute_polarization(
            self.conf.detector.polarization, px_grid, py_grid, norm_grid)

        self.qx_grid = px_grid*q_scaling/norm_grid
        self.qy_grid = py_grid*q_scaling/norm_grid
        self.qz_grid = q_scaling * \
            (self.conf.detector.distance/norm_grid - 1.)*(self.conf.detector.distance/norm_grid)

        self.solid_angle = self.conf.detector.distance * \
            (self.conf.detector.pixel_size**2) / np.power(norm_grid, 3)
        self.solid_angle *= pixel_polar

    def get_recip_coordinates(self) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """ Get coordinates of detector pixels in reciprocal space

        Returns:
        (qx, qy, qz) -- Coordinates as 2D ndarrays with pixel indexes [y, x]
        """

        return (self.qx_grid, self.qy_grid, self.qz_grid)

    def get_real_coordinates(self) -> Tuple[np.ndarray, np.ndarray]:
        """ Get coordinates of detector pixels in real space

        Returns:
        (x, y) -- Coordinates as 2D ndarrays with pixel indexes [y, x],
                  Z coordinate is always 0.
        """

        return (self.x_grid, self.y_grid)

    def get_solid_angle(self) -> np.ndarray:
        """ Get solid angle corrections for detector pixels in reciprocal space

        Returns:
        solid_angle -- 2D ndarray with pixel indexes [y, x]
        """

        return self.solid_angle
