#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Configuration module
Author: Sergey Bobkov
"""

import os
import configparser
from dataclasses import dataclass
from enum import Enum
import numpy as np

SCHEMA = {
    'Experiment': {
        'fluence': {'type': 'float'},
        'wavelength': {'type': 'float'},
        'focus_size': {'type': 'float'},
        'num_images': {'type': 'int'},
        'rot_refine': {'type': 'int'}
    },
    'Detector': {
        'resolution': {
            'type': 'list',
            'subtype': 'int',
            'len': 2
            },
        'mask': {'type': 'file'},
        'center': {
            'type': 'list',
            'subtype': 'float',
            'len': 2
            },
        'center_shift_std': {'type': 'float'},
        'distance': {'type': 'int'},
        'pixel_size': {'type': 'float'},
        'polarisation':{
            'type': 'enum',
            'values': ['x', 'y', 'none']
            },
    }
}


@dataclass(frozen=True)
class Resolution:
    'Resolution of image'
    x: int
    y: int


@dataclass(frozen=True)
class Point2D:
    '2D float coordinates for image center'
    x: float
    y: float

class Polarization(Enum):
    'Detector polarisation type'
    X = 0
    Y = 1
    none = 2

@dataclass(frozen=True)
class Detector:
    'Properties of SPI detector'
    distance: float
    pixel_size: float
    polarization: Polarization
    resolution: Resolution
    center: Point2D
    center_shift_std: float
    mask: np.ndarray


@dataclass(frozen=True)
class Experiment:
    'Properties of SPI experiment'
    num_images: int
    fluence: float
    rot_refine: int
    wavelength: float
    focus_size: float


@dataclass(frozen=True)
class Configuration:
    'Simulation configuration'
    detector: Detector
    experiment: Experiment


def read_config_data(config_file: str) -> dict:
    "Load configuration from *.ini file with configparser"

    config = configparser.ConfigParser()
    config.read(config_file)

    config_data = {}

    for section in SCHEMA:
        if not config.has_section(section):
            raise ValueError(
                "Configuration missing section '{}'".format(section))

        section_config = config[section]
        section_data = {}

        for option in SCHEMA[section]:
            if not config.has_option(section, option):
                section_data[option] = None
                continue

            option_schema = SCHEMA[section][option]
            option_type = option_schema['type']
            if option_type == 'int':
                section_data[option] = section_config.getint(option)
            elif option_type == 'float':
                section_data[option] = section_config.getfloat(option)
            elif option_type == 'list':
                elements = section_config.get(option).split(' ')
                if len(elements) != option_schema['len']:
                    raise ValueError("{} accept {} {} values as input, got {}".format(
                        option, option_schema['len'], option_schema['type'], len(elements)))

                if option_schema['subtype'] == 'int':
                    elements = [int(e) for e in elements]
                elif option_schema['subtype'] == 'float':
                    elements = [float(e) for e in elements]
                else:
                    raise ValueError("Unknown subtype {} for option {}".format(
                        option_schema['subtype'], option))

                section_data[option] = elements
            elif option_type == 'enum':
                value = section_config.get(option)
                if value not in option_schema['values']:
                    raise ValueError("{} allowed values are {}, got '{}'".format(
                        option, option_schema['values'], value))

                section_data[option] = value
            elif option_type == 'file':
                filename = os.path.join(os.path.dirname(config_file), section_config.get(option))
                if not os.path.isfile(filename):
                    raise ValueError("File doesn't exist: {}".format(filename))

                section_data[option] = filename
            else:
                raise ValueError("Unknown type {} for option {}".format(option_type, option))

        config_data[section] = section_data

    return config_data


def load_config(config_file: str) -> Configuration:
    "Load configuration from *.ini to Configuration object"

    config_data = read_config_data(config_file)

    exp_data = config_data['Experiment']
    experiment = Experiment(num_images=exp_data['num_images'],
                            fluence=exp_data['fluence'],
                            rot_refine=exp_data['rot_refine'],
                            wavelength=exp_data['wavelength'],
                            focus_size=exp_data['focus_size'])

    det_data = config_data['Detector']

    polar_str = det_data['polarisation']
    if polar_str == 'x':
        polarisation = Polarization.X
    elif polar_str == 'y':
        polarisation = Polarization.Y
    else:
        polarisation = Polarization.none

    if det_data['resolution'] is not None and det_data['mask'] is not None:
        raise ValueError(
            "Both 'resolution' and 'mask' values were provided, pick one")
    elif det_data['resolution'] is None and det_data['mask'] is None:
        resolution = None
        mask = None
    elif det_data['resolution'] is not None:
        res_x, res_y = det_data['resolution']
        resolution = Resolution(x=res_x, y=res_y)
        mask = np.zeros((res_y, res_x), dtype=np.uint8)
    else:
        mask_file = det_data['mask']
        if mask_file.split('.')[-1] != 'npy':
            raise ValueError("Mask file should have .npy format")

        mask = np.load(det_data['mask'])
        if mask.ndim != 2:
            raise ValueError(
                "Mask has wrong number of dimensions: 2 != {}".format(mask.ndim))

        res_y, res_x = mask.shape
        resolution = Resolution(x=res_x, y=res_y)

    if det_data['center'] is not None:
        cen_x, cen_y = det_data['center']
        center = Point2D(x=cen_x, y=cen_y)
    elif resolution is not None:
        center = Point2D(x=(resolution.x - 1) / 2,
                         y=(resolution.y - 1) / 2)
    else:
        center = None

    if det_data['center_shift_std'] is not None:
        center_shift_std = det_data['center_shift_std']
    else:
        center_shift_std = 0

    detector = Detector(distance=det_data['distance'],
                        pixel_size=det_data['pixel_size'],
                        polarization=polarisation,
                        resolution=resolution,
                        mask=mask,
                        center=center,
                        center_shift_std=center_shift_std)

    configuration = Configuration(detector, experiment)
    return configuration
