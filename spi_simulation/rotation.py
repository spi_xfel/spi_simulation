"""
Rotation with quaternions based on refined 600 cell
Author: Sergey Bobkov
"""

import numpy as np
from scipy.spatial.transform import Rotation as scipy_R


def _cell600_versors() -> np.ndarray:
    sixteen_cell_versors = np.vstack([np.eye(4), -np.eye(4)])
    tesseract_versors = _sing_variation(np.ones((1, 4))/2)

    phi = (1 + np.sqrt(5)) / 2
    rest_versors = np.array([[phi/2, 1/2, 1/(2*phi), 0]])
    rest_versors = _sing_variation(rest_versors)
    rest_versors = np.unique(rest_versors, axis=0)  # Remove duplicates
    rest_versors = _even_permutations(rest_versors)

    return np.vstack([sixteen_cell_versors, tesseract_versors, rest_versors])


def _sing_variation(versors: np.ndarray) -> np.ndarray:
    new_versors = np.zeros((16*len(versors), 4))

    for i in range(16):
        sign_var = np.array([i & 0x1, i & 0x2, i & 0x4, i & 0x8])
        sign_var[sign_var > 0] = 2
        sign_var -= 1

        for j, vers in enumerate(versors):
            new_versors[j*16 + i] = vers * sign_var

    return new_versors


def _even_permutations_order() -> np.ndarray:
    order = []
    for i in range(4):
        for j in range(3):
            for k in range(2):
                temp = [0]
                temp.insert(k, 1)
                temp.insert(j, 2)
                temp.insert(i, 3)

                if (i + j + k) % 2 == 0:
                    order.append(temp)

    return np.array(order, dtype=np.int64)


def _even_permutations(versors: np.ndarray) -> np.ndarray:
    order = _even_permutations_order()

    num_perm = len(order)
    num_vers = len(versors)

    result = np.zeros((num_perm*num_vers, 4))

    for i, vers in enumerate(versors):
        result[i*num_perm:(i+1)*num_perm] = np.take(vers, order)

    return result


class Rotation600Cell:
    "Provides method to create nearly uniform set of rotations in 3D space"

    def __init__(self, refinement=1):
        if refinement < 1:
            raise ValueError(
                'Refinement values < 1 is not supported, got: {}'.format(refinement))

        self._cell600 = _cell600_versors()

        self._min_dist2 = 1.1 * \
            np.sum((self._cell600 - self._cell600[0]) ** 2, axis=1)[1:].min()
        self._find_edges()
        self._find_faces()
        self._find_cells()

        self._versors = self._refine_cell600(refinement)

    def __len__(self) -> int:
        return len(self._versors)

    def __getitem__(self, index) -> scipy_R:
        if index >= len(self._versors):
            raise IndexError('index {} out of range of {} rotations'.format(
                index, len(self._versors)))

        return scipy_R.from_quat(self._versors[index])

    def _find_edges(self):
        edges = []

        num = len(self._cell600)
        self._nn_count = np.zeros((num,), dtype=np.uint8)
        self._nn_list = np.zeros((num, 12), dtype=np.uint8)

        for i in range(num):
            for j in range(i + 1, num):
                dist = np.sum((self._cell600[i] - self._cell600[j])**2)
                if dist <= self._min_dist2:
                    edges.append([i, j])

                    self._nn_list[i, self._nn_count[i]] = j
                    self._nn_list[j, self._nn_count[j]] = i
                    self._nn_count[i] += 1
                    self._nn_count[j] += 1

        self._edges = np.array(edges, dtype=np.uint8)

    def _find_faces(self):
        faces = []

        num = len(self._edges)
        num_nn = self._nn_list.shape[1]

        for i in range(num):
            for j in range(num_nn):
                if self._nn_list[self._edges[i, 0], j] <= self._edges[i, 1]:
                    continue

                idx = self._nn_list[self._edges[i, 0], j]

                dist = np.sum(
                    (self._cell600[idx] - self._cell600[self._edges[i, 1]])**2)

                if dist < self._min_dist2:
                    faces.append([self._edges[i][0], self._edges[i][1], idx])

        self._faces = np.array(faces, dtype=np.uint8)

    def _find_cells(self):
        cells = []

        num = len(self._faces)
        num_nn = self._nn_list.shape[1]

        for i in range(num):
            for j in range(num_nn):
                if (self._nn_list[self._faces[i, 0], j] <= self._faces[i, 2]):
                    continue

                idx = self._nn_list[self._faces[i, 0], j]

                dist = np.sum(
                    (self._cell600[idx] - self._cell600[self._faces[i, 1]])**2)

                if dist > self._min_dist2:
                    continue

                dist = np.sum(
                    (self._cell600[idx] - self._cell600[self._faces[i, 2]])**2)

                if dist > self._min_dist2:
                    continue

                cells.append([self._faces[i][0], self._faces[i]
                              [1], self._faces[i][2], idx])

        self._cells = np.array(cells, dtype=np.uint8)

    def _refine_cell600(self, refinement):
        ref_stack = [self._cell600]
        if refinement > 1:
            edge_points = self._refine_edges(refinement)
            ref_stack.append(edge_points)
        if refinement > 2:
            face_points = self._refine_faces(refinement)
            ref_stack.append(face_points)
        if refinement > 3:
            cell_points = self._refine_cells(refinement)
            ref_stack.append(cell_points)

        return np.vstack(ref_stack)

    def _refine_edges(self, refinement):
        num_edge = len(self._edges)
        num_new = num_edge*(refinement - 1)
        edge_points = np.zeros((num_new, 4))
        edge_point_count = 0

        for i in range(num_edge):
            ref_vec = (self._cell600[self._edges[i, 1]] -
                       self._cell600[self._edges[i, 0]]) / refinement

            for j in range(1, refinement):
                edge_points[edge_point_count] = self._cell600[self._edges[i, 0]] + j*ref_vec
                edge_point_count += 1

        return edge_points

    def _refine_faces(self, refinement):
        num_face = len(self._faces)
        num_new = num_face*(refinement - 1)*(refinement - 2) // 2
        face_points = np.zeros((num_new, 4))
        face_points_count = 0

        for i in range(num_face):
            ref_vec_v1 = (self._cell600[self._faces[i, 1]] -
                          self._cell600[self._faces[i, 0]]) / refinement
            ref_vec_v2 = (self._cell600[self._faces[i, 2]] -
                          self._cell600[self._faces[i, 0]]) / refinement

            for j in range(1, refinement - 1):
                for k in range(1, refinement - j):
                    face_points[face_points_count] = self._cell600[self._faces[i, 0]
                                                                   ] + j*ref_vec_v1 + k*ref_vec_v2
                    face_points_count += 1

        return face_points

    def _refine_cells(self, refinement):
        num_cells = len(self._cells)
        num_new = num_cells*(refinement - 1) * \
            (refinement - 2)*(refinement - 3) // 6
        cell_points = np.zeros((num_new, 4))
        cell_points_count = 0

        for i in range(num_cells):
            ref_vec_v1 = (self._cell600[self._cells[i, 1]] -
                          self._cell600[self._cells[i, 0]]) / refinement
            ref_vec_v2 = (self._cell600[self._cells[i, 2]] -
                          self._cell600[self._cells[i, 0]]) / refinement
            ref_vec_v3 = (self._cell600[self._cells[i, 3]] -
                          self._cell600[self._cells[i, 0]]) / refinement

            for j in range(1, refinement - 2):
                for k in range(1, refinement - j - 1):
                    for m in range(1, refinement - j - k):
                        cell_points[cell_points_count] = self._cell600[self._cells[i, 0]] + \
                            j*ref_vec_v1 + k*ref_vec_v2 + m*ref_vec_v3
                        cell_points_count += 1

        return cell_points
