#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Simuation module
Author: Sergey Bobkov
"""

import random
from typing import Tuple
import numpy as np
from scipy.interpolate import interpn
from scipy import signal as sp_signal
from tqdm import tqdm
from spi_processing import cxidata

from spi_simulation import config, detector, process_pdb, rotation


def compute_space_resolution(conf: config.Configuration) -> Tuple[float, float]:
    """Compute optimal voxel size for real and reciprocal space

    Keywords:
        conf -- configuration object

    Return:
        real_voxel_size -- size of voxel in real space
        recip_voxel_size -- size of voxel in reciprocal space
    """
    edge_dist_x = max(conf.detector.center.x,
                      conf.detector.resolution.x - conf.detector.center.x)
    edge_dist_y = max(conf.detector.center.y,
                      conf.detector.resolution.y - conf.detector.center.y)

    max_angle = np.arctan(np.sqrt(edge_dist_x**2 + edge_dist_y**2)
                          * conf.detector.pixel_size / conf.detector.distance)
    min_angle = np.arctan(conf.detector.pixel_size / conf.detector.distance)
    q_max = 2. * np.sin(0.5 * max_angle) * 2. * np.pi / \
        conf.experiment.wavelength
    q_min = 2. * np.sin(0.5 * min_angle) * 2. * np.pi / \
        conf.experiment.wavelength
    real_voxel_size = np.pi / q_max  # half spatial resolution
    recip_voxel_size = q_min

    return real_voxel_size, recip_voxel_size


def compute_intensity(density: np.ndarray, recip_res: int, fluence: float) -> Tuple[np.ndarray]:
    """Compute 3D intensity map from input density

    Keywords:
        density -- array with density data
        recip_res -- desired intensity resolution
        fluence -- fluence of incident radiation

    Return:
        intens -- 3D intensities in reciprocal space
    """
    pad_dens = np.zeros(3*(recip_res,))
    pad_dens[:density.shape[0],
             :density.shape[1],
             :density.shape[2]] = density.copy()

    intens = np.abs(np.fft.fftshift(np.fft.fftn(pad_dens)))**2
    if fluence is not None:
        intens *= fluence*np.power(2.81794e-9, 2)

    return intens


def compute_size(atoms: np.ndarray) -> float:
    """Compute size of a particle in Angstroms
    """
    dimensions = [atoms[:, 1].max() - atoms[:, 1].min() for i in range(1, 4)]

    return max(dimensions)


def generate_intensity_corrections(num: int,
                                   focus_size: float,
                                   particle_size: float,
                                   window_points: int = 101,
                                   window_sigma: int = 20) -> np.ndarray:
    """ Create list of possible intensity corrections

    Keywords:
        num -- number of corrections to generate
        focus_size -- size of beam focus in mm
        particle_size -- size of particle in Angstroms
        window_points -- number of point in gaussian window
        window_sigma -- sigma for gaussian window (0.5 of focus size)

    Return:
        corrections -- array of float coefficients
    """
    if particle_size > focus_size * 2e3:
        raise ValueError('Particle size is bigger than 1/5 of focus, ' +
                         'exceeding simulation capabilities.')

    window_point_size = 1e4 * focus_size / (2 * window_sigma)
    particle_points = int(particle_size / window_point_size)
    focus_1d = sp_signal.gaussian(window_points, std=window_sigma)
    focus_2d = np.matmul(focus_1d[:, np.newaxis], focus_1d[np.newaxis, :])
    focal_spot_start = window_points//2 - window_sigma
    focal_spot_end = window_points//2 + window_sigma
    focus_2d /= focus_2d[focal_spot_start:focal_spot_end,
                         focal_spot_start:focal_spot_end].mean()

    corrections = np.zeros(num, dtype=np.float64)

    for i in range(num):
        x_coors = random.randrange(window_points - particle_points)
        y_coors = random.randrange(window_points - particle_points)
        corrections[i] = focus_2d[x_coors:x_coors + particle_points,
                                  y_coors:y_coors + particle_points].mean()

    return corrections


def create_image_group(out_file: str, conf: config.Configuration):
    """ Create new image_group in cxi file with data from configuration
    """
    image_id = cxidata.add_image_group(out_file)
    cxidata.save_dataset(out_file, image_id, 'image_center',
                         np.array([conf.detector.center.x, conf.detector.center.y, 0]))
    cxidata.save_dataset(out_file, image_id, 'mask', conf.detector.mask)

    cxidata.create_dataset(out_file, image_id, 'data',
                           shape=(conf.experiment.num_images,
                                  conf.detector.resolution.y,
                                  conf.detector.resolution.x),
                           dtype=np.int32,
                           chunks=(1, conf.detector.resolution.y,
                                   conf.detector.resolution.x),
                           compression='lzf')

    cxidata.create_dataset(out_file, image_id, 'intens',
                           shape=(conf.experiment.num_images,
                                  conf.detector.resolution.y,
                                  conf.detector.resolution.x),
                           dtype=np.float64,
                           chunks=(1, conf.detector.resolution.y,
                                   conf.detector.resolution.x),
                           compression='lzf')

    return image_id


def create_pixel_data(conf: config.Configuration) -> Tuple[np.ndarray, np.ndarray]:
    """ Create q-coordinates and solid angle values for pixels

    Keywords:
        conf -- configuration

    Return:
        q_coords -- 3D array with reciprocal pixel coordinates [y, x, t],
                    where 't' have len = 3: 0 for X, 1 for Y and 2 for Z coordinate
        solid_angle -- 2D array with solid angle values for pixels.
    """
    det = detector.Detector(conf)
    qx_coords, qy_coords, qz_coords = det.get_recip_coordinates()
    q_coords = np.stack([qx_coords, qy_coords, qz_coords], axis=2)

    return q_coords, det.get_solid_angle()


def create_intens_data(atoms: np.ndarray, conf: config.Configuration, corner_q: float) -> Tuple[
        np.ndarray, Tuple[np.ndarray]]:
    """ Perform first stage of simulation: create intensities and its coords

    Keywords:
        atoms -- array with atoms data
        conf -- configuration
        resolution -- expected 3D resoultion

    Return:
        intens -- 3D array with simulated intensities
        intens_coords -- tuple of reciprocal coordinates for intens array
    """
    real_voxel_size, recip_voxel_size = compute_space_resolution(conf)

    dens = process_pdb.atoms_to_density_map(atoms, real_voxel_size)
    dens = process_pdb.low_pass_filter_density_map(dens)

    recip_res = 2 * int(corner_q/recip_voxel_size) + 3
    intens = compute_intensity(dens, recip_res, conf.experiment.fluence)
    intens_x = recip_voxel_size * (np.arange(recip_res) - (recip_res - 1) / 2)
    intens_coords = 3*(intens_x,)

    return intens, intens_coords


def simulate(pdb_file: str, conf: config.Configuration, out_file: str) -> None:
    """ Perform all simulation steps and save data
    """
    atoms = process_pdb.read_atoms(pdb_file, conf.experiment.wavelength)
    q_coords, solid_angle = create_pixel_data(conf)
    corner_q = np.sqrt((q_coords**2).sum(axis=2)).max()
    # Extend corner_q to accomodate possible shift
    if conf.detector.center_shift_std > 0:
        corner_q *= (1 + 10*conf.detector.center_shift_std / conf.detector.resolution.x)

    intens, intens_coords = create_intens_data(atoms, conf, corner_q)

    # Compute shift for 1 pixel in q_coords
    # Positive center shift correspond to negative shift in q space
    center_x = np.round(conf.detector.center.x).astype(int)
    center_y = np.round(conf.detector.center.y).astype(int)
    q_shift_pix_x = q_coords[center_y, center_x-1, 0] - \
        q_coords[center_y, center_x, 0]
    q_shift_pix_y = q_coords[center_y-1, center_x, 1] - \
        q_coords[center_y, center_x, 1]

    rot_group = rotation.Rotation600Cell(refinement=conf.experiment.rot_refine)

    cxidata.create_file(out_file)
    image_id = create_image_group(out_file, conf)

    if conf.detector.center_shift_std > 0:
        cxidata.create_dataset(
            out_file, image_id, 'simulation/true_center', (conf.experiment.num_images, 3), float)

    particle_size = compute_size(atoms)
    corrections = generate_intensity_corrections(
        conf.experiment.num_images, conf.experiment.focus_size, particle_size)

    image = np.zeros(solid_angle.shape, dtype=np.float64)
    mask_filter = (conf.detector.mask == 0)
    q_coords = q_coords[mask_filter, :]
    solid_angle = solid_angle[mask_filter]

    shift = np.zeros((3,))

    print('Generating images')
    for i in tqdm(range(conf.experiment.num_images)):
        if conf.detector.center_shift_std > 0:
            random_x_shift, random_y_shift = np.clip(
                np.random.normal(0, conf.detector.center_shift_std, size=2),
                -5*conf.detector.center_shift_std,
                5*conf.detector.center_shift_std
            )
            shift[0] = random_x_shift * q_shift_pix_x
            shift[1] = random_y_shift * q_shift_pix_y

            cxidata.update_dataset(
                out_file, image_id, 'simulation/true_center',
                np.expand_dims(shift, axis=0), i, i+1)

        rot = rot_group[random.randrange(len(rot_group))]
        rot_q_coords = rot.apply(q_coords + shift)

        values = solid_angle * interpn(intens_coords, intens, rot_q_coords)
        values *= corrections[i]
        np.place(image, mask_filter, values)
        photons = np.random.poisson(lam=image).astype(np.int32)

        cxidata.update_dataset(out_file, image_id, 'data',
                               np.expand_dims(photons, axis=0),
                               i, i+1)


def read_image_data(base_file: str, image_id: int) -> Tuple[Tuple[int], np.ndarray, Tuple[float]]:
    """ Read data shape, mask and image center values from base file

    Keywords:
        base_file -- file name for input cxi file
        image_id -- if of image group in file

    Return:
        data_shape -- shape of 'data' dataset
        mask -- values of 'mask' dataset, created all 0 if not present
        center -- values of 'image_center' dataset
    """
    dset_names = cxidata.get_names(base_file, image_id)

    if 'data' not in dset_names:
        raise ValueError(
            "Missing 'data' in file {}, image_id {}".format(base_file, image_id))

    data_shape = cxidata.get_dataset_shape(base_file, image_id, 'data')

    if len(data_shape) != 3:
        raise ValueError(
            "Expected 3-dimentional 'data' in " +
            "file {}, image_id {}, got: {}".format(base_file, image_id, data_shape))

    if 'mask' not in dset_names:
        mask = np.zeros(data_shape[1:], dtype=np.uint8)
    else:
        mask = cxidata.read_dataset(base_file, image_id, 'mask')

        if mask.shape != data_shape[1:]:
            raise ValueError(
                "Incompatible shapes for 'mask' and 'data' in " +
                "file {}, image_id {}.\n".format(base_file, image_id) +
                "{} != {}".format(mask.shape, data_shape[1:]))

    if 'image_center' not in dset_names:
        raise ValueError(
            "Missing 'image_center' in file {}, image_id {}".format(base_file, image_id))

    center = cxidata.read_dataset(base_file, image_id, 'image_center')

    return data_shape, mask, center


def simulate_match_pdb(base_file: str,
                       pdb_file: str,
                       conf: config.Configuration,
                       out_file: str,
                       scale_range: Tuple[int, int] = None) -> None:
    """ Perform all simulation steps and save data
    """

    atoms = process_pdb.read_atoms(pdb_file, conf.experiment.wavelength)

    rot_group = rotation.Rotation600Cell(refinement=conf.experiment.rot_refine)

    cxidata.create_file(out_file)

    image_ids = cxidata.get_image_groups(base_file)
    for image_id in image_ids:
        data_shape, mask, center = read_image_data(base_file, image_id)
        new_conf = config.Configuration(
            config.Detector(conf.detector.distance,
                            conf.detector.pixel_size,
                            conf.detector.polarization,
                            config.Resolution(
                                x=data_shape[2], y=data_shape[1]),
                            config.Point2D(x=center[0], y=center[1]),
                            mask),
            config.Experiment(data_shape[0],
                              conf.experiment.fluence,
                              conf.experiment.rot_refine,
                              conf.experiment.wavelength,
                              conf.experiment.focus_size))

        q_coords, solid_angle = create_pixel_data(new_conf)
        corner_q = np.sqrt((q_coords**2).sum(axis=2)).max()
        intens, intens_coords = create_intens_data(atoms, new_conf, corner_q)

        new_image_id = create_image_group(out_file, new_conf)

        generate_match_images(intens, intens_coords, q_coords, solid_angle, mask,
                              rot_group, base_file, image_id, out_file, new_image_id, scale_range)


def simulate_match_intens(base_file: str,
                          intens_data: np.ndarray,
                          voxel_size: float,
                          conf: config.Configuration,
                          out_file: str,
                          scale_range: Tuple[int, int] = None) -> None:
    """ Perform all simulation steps and save data
    """

    rot_group = rotation.Rotation600Cell(refinement=conf.experiment.rot_refine)

    cxidata.create_file(out_file)

    image_ids = cxidata.get_image_groups(base_file)
    for image_id in image_ids:
        data_shape, mask, center = read_image_data(base_file, image_id)
        new_conf = config.Configuration(
            config.Detector(conf.detector.distance,
                            conf.detector.pixel_size,
                            conf.detector.polarization,
                            config.Resolution(
                                x=data_shape[2], y=data_shape[1]),
                            config.Point2D(x=center[0], y=center[1]),
                            mask),
            config.Experiment(data_shape[0],
                              conf.experiment.fluence,
                              conf.experiment.rot_refine,
                              conf.experiment.wavelength,
                              conf.experiment.focus_size))

        q_coords, solid_angle = create_pixel_data(new_conf)

        recip_res = intens_data.shape[0]
        recip_voxel_size = voxel_size

        intens_x = recip_voxel_size * \
            (np.arange(recip_res) - (recip_res - 1) / 2)
        intens_coords = 3*(intens_x,)

        new_image_id = create_image_group(out_file, new_conf)

        generate_match_images(intens_data, intens_coords, q_coords, solid_angle, mask,
                              rot_group, base_file, image_id, out_file, new_image_id, scale_range)


def generate_match_images(intens_data, intens_coords, q_coords, solid_angle, mask,
                          rot_group, base_file, base_id, out_file, out_id, scale_range):
    "Common generation code for both pdb and intensity based matching"

    image = np.zeros(solid_angle.shape, dtype=np.float64)
    mask_filter = (mask == 0)
    q_coords = q_coords[mask_filter, :]
    solid_angle = solid_angle[mask_filter]

    print('Prepare rotations cache')
    image_points = np.zeros((len(rot_group), len(q_coords)))
    for i in tqdm(range(len(rot_group))):
        image_points[i] = solid_angle * interpn(
            intens_coords, intens_data, rot_group[i].apply(q_coords),
            bounds_error=False, fill_value=0)

    image_points /= image_points.mean(axis=1)[:, np.newaxis]

    num_images = cxidata.get_dataset_shape(base_file, base_id, 'data')[0]
    print('Simulate matching images')
    for i in tqdm(range(num_images)):
        base_image = cxidata.read_dataset(
            base_file, base_id, 'data', i, i+1)[0]
        base_points = base_image[mask_filter].astype(np.float64)
        base_intensity = base_points.mean()

        best_fit = np.argmin(
            np.abs(image_points - base_points/base_intensity).sum(axis=1))

        if scale_range is not None:
            scale_mask = np.logical_and(base_image >= scale_range[0],
                                        base_image <= scale_range[1])

            scale_mask = scale_mask[mask_filter]
            if scale_mask.sum() > 20:
                base_intensity = base_points[scale_mask].mean() / \
                    image_points[best_fit][scale_mask].mean()

        values = image_points[best_fit] * base_intensity

        np.place(image, mask_filter, values)
        photons = np.random.poisson(lam=image).astype(np.int32)

        cxidata.update_dataset(out_file, out_id, 'data',
                               np.expand_dims(photons, axis=0),
                               i, i+1)

        cxidata.update_dataset(out_file, out_id, 'intens',
                               np.expand_dims(image, axis=0),
                               i, i+1)
