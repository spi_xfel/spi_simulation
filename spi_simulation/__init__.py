"""
SPI simulation from PDB file
Author: Sergey Bobkov
"""

import os

DATA_DIR = os.path.join(os.path.dirname(__file__), 'data')
