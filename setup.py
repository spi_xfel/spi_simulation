from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='spi_simulation',
      version='0.0.1',
      description='Module for SPI simulation from PDB file.',
      long_description=readme(),
      long_description_content_type="text/markdown",
      url='https://gitlab.com/spi_xfel/spi_simulation',
      author='Sergey Bobkov',
      author_email='s.bobkov@grid.kiae.ru',
      license='MIT',
      python_requires='>=3.6',
      install_requires=['numpy',
                        'scipy',
                        'tqdm',
                        'spi_processing'],
      packages=['spi_simulation'],
      scripts=['scripts/spi_simulate.py',
               'scripts/spi_simulate_defaults.py',
               'scripts/spi_simulate_match.py'],
      include_package_data=True,
      zip_safe=False)
