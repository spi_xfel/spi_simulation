#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Simulate SPI images from PDB file
Author: Sergey Bobkov
"""

from dataclasses import fields
import os
import argparse

from spi_simulation import config, process_pdb, simulation


def test_config(conf: config.Configuration):
    """Test that all required fields are not None
    """
    for s_name in ['experiment', 'detector']:
        s_data = getattr(conf, s_name)
        for field in fields(s_data):
            if getattr(s_data, field.name) is None:
                raise ValueError("Missing required option '{}' in '{}'".format(field.name, s_name))


def main():
    """Parse input params and start simulations
    """
    parser = argparse.ArgumentParser(
        description='Simulate SPI images from PDB file')
    parser.add_argument(
        'config_file', help='File with experiment configuration')
    parser.add_argument('-c', '--code', help='PDB code')
    parser.add_argument('-f', '--file', help='PDB file')
    parser.add_argument('-o', '--out', help='Output file')
    parser_args = parser.parse_args()

    config_file = parser_args.config_file
    pdb_code = parser_args.code
    pdb_file = parser_args.file
    out_file = parser_args.out

    if not os.path.isfile(config_file):
        parser.error("File {} doesn't exist".format(config_file))

    if pdb_code is None and pdb_file is None:
        parser.error("Neither PDB-code nor PDB-file were provided")
    elif pdb_code is not None and pdb_file is not None:
        parser.error("Both PDB-code and PDB-file were provided, pick one")
    elif pdb_code is not None:
        pdb_file = process_pdb.fetch_pdb(pdb_code)
    else:
        pdb_code = os.path.basename(pdb_file).split('.')[0].upper()

    if not os.path.isfile(pdb_file):
        parser.error("File {} doesn't exist".format(pdb_file))

    if out_file is None:
        out_file = 'simulated_{}.cxi'.format(pdb_code)

    if os.path.exists(out_file):
        parser.error("Output file {} already exists".format(out_file))

    configuration = config.load_config(config_file)
    test_config(configuration)

    simulation.simulate(pdb_file, configuration, out_file)


if __name__ == '__main__':
    main()
