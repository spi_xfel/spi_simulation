#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Create default simulation files
Author: Sergey Bobkov
"""

import os
import argparse
from shutil import copyfile

from spi_simulation import DATA_DIR


def main():
    """Create default simulation files
    """
    parser = argparse.ArgumentParser(
        description='Create default simulation files')
    parser.add_argument('-o', '--out', default='.', help='Output directory')
    parser_args = parser.parse_args()

    out_dir = parser_args.out

    if not os.path.isdir(out_dir):
        parser.error("Directory '{}' doesn't exist".format(out_dir))

    config_filename = 'config.ini'
    det_dirname = 'masks'

    if os.path.exists(config_filename):
        parser.error("File '{}' already exist".format(config_filename))
    if os.path.exists(det_dirname):
        parser.error("Directory '{}' already exist".format(det_dirname))

    copyfile(os.path.join(DATA_DIR, 'default.ini'),
             os.path.join(out_dir, config_filename))
    os.symlink(os.path.join(DATA_DIR, det_dirname),
               os.path.join(out_dir, det_dirname))

    print("Successfully created '{}' and '{}' in '{}'".format(
        config_filename, det_dirname, os.path.abspath(out_dir)))


if __name__ == '__main__':
    main()
