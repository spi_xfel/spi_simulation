#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Simulate SPI images from PDB file
Author: Sergey Bobkov
"""

import os
import sys
import argparse
import numpy as np
from spi_processing import cxidata

from spi_simulation import config, process_pdb, simulation

def test_config(conf: config.Configuration):
    """Test that all required fields are not None
    """
    exp_data = conf.experiment
    for f_name in ['wavelength', 'rot_refine']:
        if getattr(exp_data, f_name) is None:
            raise ValueError("Missing required option '{}' in 'Experiment'".format(f_name))

    det_data = conf.detector
    for f_name in ['distance', 'pixel_size', 'polarization']:
        if getattr(det_data, f_name) is None:
            raise ValueError("Missing required option '{}' in 'Detector'".format(f_name))


def main():
    """Parse input params and start simulations
    """
    parser = argparse.ArgumentParser(
        description='Simulate SPI images that match existing data')
    parser.add_argument(
        'config_file', help='File with experiment configuration')
    parser.add_argument(
        'base_file', help='File with existing images')
    parser.add_argument('-c', '--code', help='PDB code')
    parser.add_argument('-f', '--file', help='PDB file')
    parser.add_argument('-i', '--intens', help='3D intensity')
    parser.add_argument('-v', '--voxel', type=float, help='3D voxel size [inverse Angstrom]')
    parser.add_argument('-r', '--range', metavar='MIN:MAX',
                        help='Intensity range considered for scaling')
    parser.add_argument('-o', '--out', help='Output file')
    parser_args = parser.parse_args()

    config_file = parser_args.config_file
    base_file = parser_args.base_file
    pdb_code = parser_args.code
    pdb_file = parser_args.file
    intens_file = parser_args.intens
    voxel = parser_args.voxel
    scale_range = parser_args.range
    out_file = parser_args.out

    if not os.path.isfile(config_file):
        parser.error("File {} doesn't exist".format(config_file))

    if not os.path.isfile(base_file):
        parser.error("File {} doesn't exist".format(base_file))

    if scale_range is not None:
        split_range = scale_range.split(':')
        if len(split_range) != 2:
            parser.error("Range format is 'MIN:MIN', got '{}'".format(scale_range))
        sr_min = int(split_range[0])
        sr_max = int(split_range[1])
        scale_range = (sr_min, sr_max)

    if out_file is None:
        out_file = 'simulated.cxi'

    if os.path.exists(out_file):
        parser.error("Output file {} already exists".format(out_file))

    configuration = config.load_config(config_file)
    test_config(configuration)

    sys.stderr.write("Warning: 'mask', 'resolution' and 'center' will be ignored, " + \
                     "using parameters from base file")

    if (pdb_code is not None) + (pdb_file is not None) + (intens_file is not None) != 1:
        parser.error("Wrong inputs, pick one between PDB code, PDB file or 3D intensity")

    if pdb_code is not None or pdb_file is not None:
        if pdb_code is not None:
            pdb_file = process_pdb.fetch_pdb(pdb_code)
        simulation.simulate_match_pdb(
            base_file, pdb_file, configuration, out_file, scale_range)
    else:
        filetype = intens_file.split('.')[-1]
        intens_data = None
        if filetype == 'npy':
            intens_data = np.load(intens_file)
        elif filetype == 'cxi':
            imid = cxidata.get_image_groups(intens_file)[0]
            intens_data = cxidata.read_dataset(intens_file, imid, 'data')
        else:
            raise ValueError('Unknown data format for intensities: {}'.format(filetype))

        if voxel is None:
            parser.error("Voxel size is required by intensity match, but it's not defined")

        simulation.simulate_match_intens(
            base_file, intens_data, voxel, configuration, out_file, scale_range)


if __name__ == '__main__':
    main()
